# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:
    __PREFIX__node:
        volumes:
            - __PREFIX__project-sync:/usr/src/service:nocopy
        expose:
            - "13136"


volumes:
    __PREFIX__project-sync:
        external: true
