# Инструкция по установке и настройке PHP окружения altrecipe

## 1. Установить докер и докер-компоуз
### 1.1. Для мака:

[Инструкция](https://docs.docker.com/docker-for-mac/install/)

После установки докера на мак нужно поставить еще docker-sync:
```
gem install docker-sync
```

### 1.2. Для ubuntu (используем ubuntu 18.04):

сначала ставим сам докер проходимся по пункту 1(и 2 по желанию):

[Инструкция](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

Потом ставим docker-compose(здесь выполняем только шаг 1):

[Инструкция](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)

## 2. Запускаем скрипт разворачивания докера:

```
curl https://gitlab.com/ar-public/node_env/raw/master/ndenv.sh -k --output ndenv.sh && chmod +x ndenv.sh && ./ndenv.sh -p
```



Скрипт создаст нужную структуру папок и установит необходимые докер имейджи.

После выполнения скрипта у тебя будет 2 папки - docker и project
в папке docker находятся файлы ,l. Если что - папку mysql можно бекапить и переносить на другой такой же докер.

В папке project разворачиваешь нужные тебе приложения и тд.

Запустить имейджи можно командами:
<strong>Для сервера:</strong>
```
./ndenv.sh -s
```
<strong>На рабочей машине:</strong>
```
./ndenv.sh -sd
```

# 3. Документация по скрипту:

Вся документация по скрипту находится helpe самгого скрипта:
```
./ndenv.sh -h
```
Или запустить `./ndenv.sh` без параметров

Вот дубликат документации скрипта, для более удобного просмотра:
```
       -s, --run, --start        run containers
       -sd, --rund, --startd     run containers on development machine
       -p, --setup               prepare folder structure and containers
       -d, --stop                stop running containers
       -r, --reboot, --restart   restart containers
       --clean                   clean containers cache
       --clean-run               reboot containers with clean
       --clean-docker            clean all docker stuff, but leave mysql DB and project
       --clean-full              DANGER!!!! remove all folders, including database and your project
       --rebuild                 clean docker cache and rebuild
       --shell-node              open comand line for NODE.JS docker container
       --shell-db                open comand line for MYSQL docker container
       --help, -h                show this manual

```






















