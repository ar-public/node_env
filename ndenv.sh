#!/bin/bash

project_slug=""
prefix=""

space() {
	echo ""
	echo ""
	echo ""
}

create_dirs() {
	echo "Creating directories structure"
	mkdir -p docker/mysql &&
	mkdir project
	echo "Directories created"
}

download_configs() {
	echo "Downloading configs:" 
	space
	echo "docker-compose:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/docker-compose.tpl -k --output docker-compose.tpl
	space
	echo "docker-compose-dev:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/docker-compose-dev.tpl -k --output docker-compose-dev.tpl
	space
	echo "docker-sync:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/docker-sync.tpl -k --output docker-sync.tpl
	echo "docker-sync:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/docker-sync.tpl -k --output docker-sync.tpl
	space
	echo "package.json:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/package.json -k --output project/package.json
	space
	echo "index.js:" 
	curl https://gitlab.com/ar-public/node_env/raw/master/index.js -k --output project/index.js
	space
	echo "Configs downloaded" 
}

set_project_slug() {
	read -p 'Enter project shortname(lowercase): ' pName
	project_slug=$pName
	prefix=$pName"_"
	echo $prefix > conf
}

replace() {
	local from=${1}
    local to=${2}
    local file=${3}

    unameOut="$(uname -s)"
	case "${unameOut}" in
	    Linux*)
		sed -i "s/$from/$to/g" $file
		;;
	    Darwin*)
		sed -i '' "s/$from/$to/g" $file
		;;
	    *)
		echo "UNSUPORTED OS!"
		exit 1
	esac
}

replace_docker() {
	replace ${1} ${2} 'docker-compose.yml'
}

replace_docker_dev() {
	replace ${1} ${2} 'docker-compose-dev.yml'
}

replace_sync() {
	replace ${1} ${2} 'docker-sync.yml'
}

replace_package() {
	echo "replacing package.json"
	replace ${1} ${2} 'project/package.json'
}

setup_docker_compose() {
	placeholder="__PREFIX__"
	replace_docker $placeholder $prefix
	replace_docker_dev $placeholder $prefix
	replace_sync $placeholder $prefix
	replace_package $placeholder $project_slug
}

setup_mysql() {
	read -p 'Set dbname for postgresql: ' dbname
	read -p 'Set username for postgresql: ' username
	read -p 'Set userpass for postgresql: ' userpass

	replace_docker "__MYSQL_DBNAME__" $dbname
	replace_docker "__MYSQL_DBUSER__" $username
	replace_docker "__MYSQL_DBPASS__" $userpass
}

clean_compose() {
	yes | cp -rf docker-compose.tpl docker-compose.yml
}

clean_sync() {
	yes | cp -rf docker-sync.tpl docker-sync.yml
}

clean_compose_dev() {
	yes | cp -rf docker-compose-dev.tpl docker-compose-dev.yml
}

claen_ylms() {
	clean_compose
	clean_compose_dev
	clean_sync
}


#########################
# The command line help #
#########################
display_help() {
    echo "Usage: $0 [option...]" >&2
    echo
    echo "   -s, --run, --start		run containers"
    echo "   -sd, --rund, --startd	run containers on development machine"
    echo "   -p, --setup              	prepare folder structure and containers"
    echo "   -d, --stop              	stop running containers"
    echo "   -r, --reboot, --restart 	restart containers"
    echo "   --clean              	clean containers cache"
    echo "   --clean-run              	reboot containers with clean"
    echo "   --clean-docker              	clean all docker stuff, but leave mysql DB and project"
    echo "   --clean-full              	DANGER!!!! remove all folders, including database and your project"
    echo "   --rebuild              	clean docker cache and rebuild"
    echo "   --shell-node              	open comand line for NODE.JS docker container"
    echo "   --shell-db              	open comand line for MYSQL docker container"
    echo "   --help, -h              	show this manual"
    echo
    # echo some stuff here for the -a or --add-options 
    exit 1
}


argument="$1"
comand_prefix=$(cat conf)

case $argument in
	--mkdirs )
	create_dirs
	exit 0
	;;
	--download )
	download_configs
	;;
	-p|--setup )
	clear
	create_dirs
	download_configs
	claen_ylms
	set_project_slug
	setup_docker_compose
	setup_mysql
	echo "Now we are going to build images...." 
	docker-compose build
	space
	echo "ALL DONE!!!" 
	;;
	--clean)
	docker-compose down
	docker-compose rm
	;;
	--clean-full)
	docker-compose down
	docker-compose rm
	rm -rf docker
	rm -rf project
	;;
	--clean-run)
	docker-compose down
	docker-compose rm
	docker-compose build
	docker-compose up -d
	;;
	--clean-docker)
	docker-compose down
	docker-compose rm
	rm -rf docker/php
	rm -rf docker/nginx
	rm -rf docker-compose.yml
	rm -rf docker-compose-dev.yml
	rm -rf docker-sync.yml
	rm -rf conf
	;;
	--rebuild)
	docker-compose down
	docker-compose rm
	docker-compose build
	;;
	-s|--run|--start)
	docker-compose up -d
	;;
	-sd|--rund|--startd)
	docker-sync-stack start
	;;
	--conf-nginx)
	nano docker/nginx/core/nginx.conf
	;;
	-r|--reboot|--restart)
	docker-compose stop
	docker-compose up -d
	;;
	-d|--stop)
	docker-compose stop
	;;
	--conf-php)
	nano docker/php/php.ini
	;;
	--shell-php)
	docker exec -ti "$comand_prefix"php /bin/bash
	;;
	--fix-dev)
	docker exec -ti "$comand_prefix"php chown www-data:www-data runtime -R /var/www
	;;
	--shell-php-dev)
	docker exec -ti "$comand_prefix"php chown www-data:www-data runtime -R /var/www
	docker exec -ti "$comand_prefix"php /bin/bash
	;;
	--shell-db)
	docker exec -ti "$comand_prefix"db /bin/bash
	;;
	-h|--help)
	display_help
	;;
	--shell-nginx)
	docker exec -ti "$comand_prefix"nginx /bin/bash
	;;
	*)
	# set_project_slug
	# setup_docker_compose
	# setup_mysql
	display_help
	# clean_compose
	# claen_ylms
	# set_project_slug
	# setup_docker_compose
esac


