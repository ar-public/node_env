# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:

    __PREFIX__db:

        image: postgres:10

        ports:
            - "54320:5432"

        container_name: __PREFIX__db
        environment:
            - POSTGRES_USER=__MYSQL_DBUSER__
            - POSTGRES_PASSWORD=__MYSQL_DBPASS__
            - POSTGRES_DB=__MYSQL_DBNAME__


        volumes:
            - ./docker/mysql:/var/lib/postgresql/


    __PREFIX__node:
        image: node:latest
        ports:
            - "1234:1234"
        container_name: __PREFIX__node
        command: "npm start"
        volumes:
            - ./project:/usr/src/service
        working_dir: /usr/src/service
        links:
            - __PREFIX__db:db


volumes:
    db-data:
        driver: local
